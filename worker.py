import socket
import threading
import datetime
import pytz

HEADER = 64
PORT = 5050
# SERVER = socket.gethostbyaddr("54.144.208.228")[0]
SERVER = socket.gethostbyname(socket.gethostname())
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = "!DISCONNECT"

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(ADDR)


def handle_client(conn, addr):
    print(f"[NEW CONNECTION] {addr} connected.")

    connected = True
    while connected:
        msg_length = conn.recv(HEADER).decode(FORMAT)
        if msg_length:
            msg_length = int(msg_length)
            msg = conn.recv(msg_length).decode(FORMAT)
            if msg == DISCONNECT_MESSAGE:
                connected = False
                continue

            print(f"[{addr}] {msg}")
            conn.send(get_time(msg).encode(FORMAT))

    conn.close()


def get_time(command):
    command = command.lower()
    current_time = datetime.datetime.now(pytz.timezone('Asia/Jakarta'))
    if command == "datetime":
        return f"[Jakarta] {current_time.hour}:{current_time.minute}:{current_time.second}, {current_time.day}-{current_time.month}-{current_time.year}\nFINISHED"
    if command == "time":
        return f"[Jakarta] {current_time.hour}:{current_time.minute}:{current_time.second}\nFINISHED"
    if command == "date":
        return f"[Jakarta] {current_time.day}-{current_time.month}-{current_time.year}\nFINISHED"
    return "FAILED"


def start():
    server.listen()
    print(f"[LISTENING] Server is listening on {SERVER}")
    while True:
        conn, addr = server.accept()
        thread = threading.Thread(target=handle_client, args=(conn, addr))
        thread.start()
        print(f"[ACTIVE CONNECTIONS] {threading.activeCount() - 1 }")


print("[STARTING] server is starting...")
start()
