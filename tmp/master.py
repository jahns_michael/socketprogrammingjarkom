import socket
import threading

HEADER = 64
PORT = 5050
SERVER = socket.gethostbyname(socket.gethostname())
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = "!DISCONNECT"

workers_addr = [SERVER]

workers_socket = []

master = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
master.bind(ADDR)


def handle_client(conn, addr):
    print(f"[NEW CONNECTION] {addr} connected.")

    connected = True
    while connected:
        req = conn.recv(2048).decode(FORMAT)
        conn.send("Request received".encode(FORMAT))
        if req == DISCONNECT_MESSAGE:
            connected = False
        elif req == "STATE":
            conn.send(get_workers_state().encode(FORMAT))
        elif req == "JOB":
            handle_job(conn)
        print(f"[{addr}] {req}")

    conn.close()


def handle_job(client):
    workers_socket[0].send("JOB".encode(FORMAT))
    client.send(workers_socket[0].recv(2048).decode(FORMAT).encode(FORMAT))


def get_workers_state():
    all_state = ""
    for worker in workers_socket:
        worker.send("STATE".encode(FORMAT))
        all_state += f"{worker.recv(2048).decode(FORMAT)}\n"
    return all_state


def start():
    master.listen()
    print(f"[LISTENING] Master is listening on {SERVER}")
    while True:
        conn, addr = master.accept()
        sock_type = conn.recv(2048).decode(FORMAT)
        if sock_type.split(" ")[0] == "WORKER":
            workers_socket.append(conn)
        elif sock_type.split(" ")[0] == "CLIENT":
            thread = threading.Thread(target=handle_client, args=(conn, addr))
            thread.start()
            print(f"[ACTIVE CONNECTIONS] {threading.activeCount() - 1}")


print("[STARTING] master is starting...")
start()
