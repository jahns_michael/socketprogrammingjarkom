import socket
import time
import threading

HEADER = 64
PORT = 5050
SERVER = "192.168.56.1"
MASTER = (SERVER, 5050)
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
JOB_MESSAGE = "JOB"
STATE_MESSAGE = "STATE"
NAME = "W2"
MAX_THREAD = 2

worker = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
worker.connect(ADDR)

working = False


def work(command):
    global working
    if command == STATE_MESSAGE:
        worker.send(
            f"WORKER {NAME} {'BUSY' if working else 'READY'}".encode(FORMAT))
    elif command == JOB_MESSAGE:
        if not working:
            working = True
            worker.send(
                f"[{NAME} START] Hello from {NAME}. Connected for 60 secs".encode(FORMAT))
            time.sleep(60)
            worker.send(f"[{NAME} DONE] 60 secs passed.".encode(FORMAT))
            working = False
        else:
            worker.send(
                f"[{NAME} BUSY] Sorry, {NAME} still have some task to do now.".encode(FORMAT))
    else:
        worker.send(
            f"[{NAME} UNKNOWN] Unknown command.".encode(FORMAT))


def start():
    worker.send(
        f"WORKER".encode(FORMAT))
    while True:
        command = worker.recv(2048).decode(FORMAT)
        if command:
            work(command)


print(f"[STARTING] worker [{NAME}] is starting...")
start()
