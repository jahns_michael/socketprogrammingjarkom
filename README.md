# Tugas 3 : Socket Programming
Kelompok B4.
Anggota:
1. Jahns Michael (1806141252)
2. Reka Paska Enda (1806186793)
3. Saul Andre Lumban Gaol (1706023555)

## Overview
Script yang kami buat untuk tugas kali ini adalah script untuk mengetahui waktu dan tanggal di zona Jakarta. Kami memakai bahasa pemrograman Python dan memanfaatkan module built-in yaitu socket dan threading.

## Definisi Master Worker
Master adalah script yang berjalan di PC lokal user. Dalam hal ini Master berperan sebagai client pada arsitektur client-server. Worker adalah script yang berjalan di EC2. Dalam hal ini Worker berperan sebagai server pada arsitektur client-server.

## Definisi Job
Pada tugas kali ini worker dapat melakukan job yang akan mereturn waktu di jakarta saat diminta, terdapat 3 jenis perintah yang bisa dilakukan:

![img01](img/img01.png)

- time: akan mereturn waktu jakarta dalam format:
    > `[Jakarta] hour:minute:second`
- date: akan mereturn tanggal jakarta dalam format:
    > `[Jakarta] day-month-year`
- datetime: akan mereturn waktu dan tanggal dalam format:
    > `[Jakarta] hour:minute:second, day-month-year`

## Fitur yang dikerjakan
Pada tugas kali ini kelompok kami hanya dapat mengerjakan 3 fitur dasar yang diwajibkan. 
1. Terdapat sebuah master node local yang menerima perintah dari user. Job tersebut diberikan langsung ke node master ini.
Terdapat tiga perintah yang dapat diterima oleh master node. 
   ```python
   def get_time(command):
       command = command.lower()
       current_time = datetime.datetime.now(pytz.timezone('Asia/Jakarta'))
       if command == "datetime":
           return f"[Jakarta] {current_time.hour}:{current_time.minute}:{current_time.second}, {current_time.day}-{current_time.month}-{current_time.year}"
       if command == "time":
           return f"[Jakarta] {current_time.hour}:{current_time.minute}:{current_time.second}"
       if command == "date":
           return f"[Jakarta] {current_time.day}-{current_time.month}-{current_time.year}"
       return "FAILED"
   ```
   - datetime : me-return jam dan tanggal Jakarta saat ini(dalam format `hh:mm:ss`, `dd:mm:yyyy`).
   - time : me-return jam saat ini (dalam format `hh:mm:ss`).
   - date : me-return tanggal saat ini (dalam format `dd:mm:yyyy`).
  
2. Terdapat sebuah worker yang berjalan di aws EC2
    ```console
    // server-side
    [STARTING] server is starting...
   [LISTENING] Server is listening on <hostname>
   [NEW CONNECTION] (<masterhostname>, 59188) connected.
   [ACTIVE CONNECTIONS] 1
   [(<masterhostname>, <port>)] date
   [(<masterhostname>, <port>)] time
   [(<masterhostname>, <port>)] datetime
   [(<masterhostname>, <port>)] !DISCONNECT   
    ```
3. Adanya komunikasi antara worker dan master node.
   ![img02](img/img02.png)
   > Gambar di atas adalah job yang diberikan dari master (client)

   ```console
   // di server
   [(<masterhostname>, <port>)] date
   [(<masterhostname>, <port>)] time
   [(<masterhostname>, <port>)] datetime
   ```

## Fitur Elektif
1. Menampilkan status ketersediaan suatu worker yang dapat di-assign dengan sebuah task.
    ```console
    $ python3 master.py
    Worker(<workerhostname>, <port>) W1 is DEAD
    Worker(<workerhostname>, <port>) W2 is ACTIVE
    ```
    Keterangan: Worker pertama (W1) sengaja di matikan di EC2, master menginformasikan ke terminal jika gagal melakukan koneksi.
2. Menampilkan worker mana yang menjalankan task
    ```console
    Worker('54.235.46.157', 5050) W1 is DEAD
    Worker('54.236.5.149', 5050) W2 is ACTIVE
    Message: date
    Worker: W2
    [Jakarta] 25-10-2020
    Finished
    ```
    Dalam kasus ini, master meminta user untuk memilih worker mana yang ingin diberikan task.
3. Menampilkan status task yang diberikan ke worker.
    ```console
    Worker W1 is not available, status DEAD
    Worker('54.235.46.157', 5050) W1 is DEAD
    Worker('54.236.5.149', 5050) W2 is ACTIVE
    Message: date
    Worker: W2
    [Jakarta] 25-10-2020
    Finished
    Worker('54.235.46.157', 5050) W1 is DEAD
    Worker('54.236.5.149', 5050) W2 is ACTIVE
    Message: tanggal
    Worker: W2
    FAILED
    ```
    Status Finished jika berhasil, FAILED jika gagal.

4. Membatalkan  hubungan dengan worker, master memustukan hubungan dengan worker.
    ```console
    Worker('54.235.46.157', 5050) W1 is DEAD
    Worker('54.236.5.149', 5050) W2 is ACTIVE
    Message: !DISCONNECT
    Worker: W2
    ```
    Task yang diberikan akan selalu berjalan karena tidak memakan waktu lama, namun koneksi bisa diputus dengan perintah `!DISCONNECT`.


[Link Repository Gitlab](https://gitlab.com/jahns_michael/socketprogrammingjarkom)




