import socket

HEADER = 64
PORT = 5050
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = "!DISCONNECT"

workers = [
    {
        "addr": ("54.235.46.157", PORT),
        "status": "DEAD",
        "name": "W1",
        "socket": None
    },
    {
        "addr": ("54.236.5.149", PORT),
        "status": "DEAD",
        "name": "W2",
        "socket": None
    }
]


def connect():
    for worker in workers:
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            if worker['status'] == "DEAD":
                client.connect(worker["addr"])
                worker["status"] = "ACTIVE"
                worker["socket"] = client
        except Exception as e:
            pass
        print(f"Worker{worker['addr']} {worker['name']} is {worker['status']}")


def send(msg, client):
    message = msg.encode(FORMAT)
    msg_length = len(message)
    send_length = str(msg_length).encode(FORMAT)
    send_length += b' ' * (HEADER - len(send_length))
    client.send(send_length)
    client.send(message)
    print(client.recv(2048).decode(FORMAT))


def get_status():
    for worker in workers:
        print(f"Worker{worker['addr']} is {worker['status']}")


def get_worker_byname(name):
    for worker in workers:
        if worker["name"] == name:
            return worker
    return None


while True:
    connect()
    msg = input("Message: ")
    worker_name = input("Worker: ")
    worker = get_worker_byname(worker_name)

    if worker != None:
        if worker["status"] == "ACTIVE":
            send(msg, worker['socket'])
        elif worker["status"] == "DEAD":
            print(
                f"Worker {worker['name']} is not available, status {worker['status']}")
    else:
        print(f"Invalid worker with name {worker_name}")

    if (msg == DISCONNECT_MESSAGE):
        break
